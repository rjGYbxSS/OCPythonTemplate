FROM ocdr/dkube-datascience-tf-cpu:v1.13
ADD . /code
WORKDIR /code
RUN pip3 install flask
CMD ["python3", "app.py"]
EXPOSE 5000
